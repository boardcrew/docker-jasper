FROM raspbian/stretch

MAINTAINER Fernando Hackbart <fhackbart@gmail.com>

WORKDIR /root

ENV DEBIAN_FRONTEND noninteractive
ENV JASPER_HOME /opt/jasper
RUN export JASPER_HOME=$JASPER_HOME 

COPY files/jasper.conf /lib/modprobe.d/jasper.conf

RUN apt-get -y update && \
    apt-get -y install dirmngr 
COPY files/experimental.list /etc/apt/sources.list.d/experimental.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8B48AD6246925553 && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 7638D0442B90D010 && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 04EE7237B7D453EC && \
    apt-get -y update && \
    apt-get -y install nano git-core python-dev bison  libasound2-dev libportaudio-dev python-pyaudio python-pip python-dev build-essential pocketsphinx python-pocketsphinx subversion autoconf libtool automake gfortran g++ espeak mpg123 mpg321 python-pymad flite libttspico-utils festival festvox-don && \
    apt-get -y install -t experimental phonetisaurus m2m-aligner mitlm libfst-tools && \
    apt-get clean

RUN pip install --upgrade pip==9.0.3 && \
    pip install --upgrade virtualenv && \
    pip install --upgrade setuptools && \
    pip install --upgrade gTTS && \
    pip install --upgrade pyvona

WORKDIR /opt/jasper
RUN svn checkout https://svn.code.sf.net/p/cmusphinx/code/trunk/cmuclmtk/
WORKDIR /opt/jasper/cmuclmtk
RUN ./autogen.sh && \
    make && \
    make install

WORKDIR /opt/jasper
RUN wget -q https://www.dropbox.com/s/kfht75czdwucni1/g014b2b.tgz && \
    tar xzf g014b2b.tgz && \
    rm g014b2b.tgz
WORKDIR /opt/jasper/g014b2b
RUN ./compile-fst.sh
WORKDIR /opt/jasper
RUN mv g014b2b phonetisaurus

RUN mkdir -p models
COPY files/hub4wsj_sc_8k.tar.gz /opt/jasper/models/hub4wsj_sc_8k.tar.gz
WORKDIR /opt/jasper/models
RUN tar xzf hub4wsj_sc_8k.tar.gz && \
    rm hub4wsj_sc_8k.tar.gz

WORKDIR /opt/jasper
RUN git clone -b jasper-dev https://github.com/jasperproject/jasper-client.git && \
    mv jasper-client jasper
WORKDIR /opt/jasper/jasper
RUN ./setup.py build && \
    ./setup.py install && \ 
    chmod 755 Jasper.py 
    
ENTRYPOINT ["./Jasper.py"]

