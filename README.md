

# docker-jasper-instalador

## build image
```
git clone https://gitlab.com/boardcrew/docker-jasper.git
cd docker-jasper
docker build -t docker-jasper .
```

## run the container

Create a `profile.yml` with your preferences for Jasper and point to te folder in the volume `/home/pi/.jasper` in the example below 

 ```
docker run -ti --privileged \
      -v /dev/snd/pcmC0D0p:/dev/snd/pcmC0D0p \
      -v /dev/snd/pcmC1D0c:/dev/snd/pcmC1D0c \
      -v /dev/snd/controlC0:/dev/snd/controlC0 \
      -v /dev/snd/controlC1:/dev/snd/controlC1 \
      -v /home/pi/.jasper:/root/.jasper \
      boardcrew/docker-jasper
```      
